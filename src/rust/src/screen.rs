use crate::ports;

const VIDEO_ADDRESS: usize = 0xb8000;
const MAX_ROWS: isize = 25;
const MAX_COLS: isize = 80;
const DEFAULT_TEXT_COLOR: u8 = color(CL_LT_GREEN, CL_BLACK);
const DEFAULT_ERR_COLOR: u8 = color(CL_RED, CL_WHITE);
const CL_BLACK: u8 = 0x00;
const CL_BLUE: u8 = 0x01;
const CL_GREEN: u8 = 0x02;
const CL_CYAN: u8 = 0x03;
const CL_RED: u8 = 0x04;
const CL_MAGENTA: u8 = 0x05;
const CL_BROWN: u8 = 0x06;
const CL_LT_GRAY: u8 = 0x07;
const CL_GRAY: u8 = 0x08;
const CL_LT_BLUE: u8 = 0x09;
const CL_LT_GREEN: u8 = 0x0a;
const CL_LT_CYAN: u8 = 0x0b;
const CL_LT_RED: u8 = 0x0c;
const CL_LT_MAGENTA: u8 = 0x0d;
const CL_YELLOW: u8 = 0x0e;
const CL_WHITE: u8 = 0x0f;

// Screen i/o ports
const PORT_SCREEN_CTRL: u16 = 0x3d4;
const PORT_SCREEN_DATA: u16 = 0x3d5;
const CTRL_CURSOR_HI: u8 = 14;
const CTRL_CURSOR_LO: u8 = 15;

pub type OSChar = u8;

pub struct ScreenLoc {
    pub row: u8,
    pub col: u8,
}

pub const fn color(fg: u8, bg: u8) -> u8 {
    bg << 4 | fg
}

pub fn clear_screen() {
    let screen_size = MAX_COLS * MAX_ROWS * 2;
    let video_memory = VIDEO_ADDRESS as *mut u8;

    for offset in 0..screen_size {
        unsafe {
            *video_memory.offset(offset) = 0x00;
        }
    }
    set_cursor_offset(0);
}

pub fn kprint_at<T>(message: &[OSChar], loc: T) where T: Into<Option<ScreenLoc>> {
    // get/set cursor characteristics
    let loc = loc.into();
    let (mut row, mut col) = if loc.is_some() {
        let l = loc.unwrap();
        (l.row, l.col)
    } else {
        let o = get_cursor_offset();
        (get_offset_row(o), get_offset_col(o))
    };

    // loop through message and print it
    let mut offset;
    for ch in message {
        offset = print_char(*ch, col, row, DEFAULT_TEXT_COLOR);
        row = get_offset_row(offset);
        col = get_offset_col(offset);
    }
}

pub fn kprint(message: &[OSChar]) {
    kprint_at(message, None);
}

fn print_char(ch: OSChar, col: u8, row: u8, attr: u8) -> isize {
    let video_memory = VIDEO_ADDRESS as *mut u8;

    // print a red 'E' if coords are out of bounds
    if col >= MAX_COLS as u8 || row >= MAX_ROWS as u8 {
        let offset = 2 * MAX_COLS * MAX_ROWS - 2;
        unsafe {
            *video_memory.offset(offset) = 'E' as u8;
            *video_memory.offset(offset + 1) = DEFAULT_ERR_COLOR;
        }
        return get_offset(col, row);
    }

    let mut offset = get_offset(col, row);
    if ch == '\n' as u8 {
        offset = get_offset(0, get_offset_row(offset) + 1);
    } else {
        unsafe {
            *video_memory.offset(offset) = ch;
            *video_memory.offset(offset + 1) = attr;
        }
        offset += 2;
    }

    if offset >= MAX_ROWS * MAX_COLS * 2 {
        unsafe {
            core::ptr::copy(video_memory.offset(MAX_COLS * 2), video_memory, ((MAX_ROWS - 1) * MAX_COLS * 2) as usize);
        }
        for i in 0..(MAX_COLS as u8) {
            let o = get_offset(i, MAX_ROWS as u8 - 1);
            unsafe {
                *video_memory.offset(o) = 0x00;
            }
        }
        offset = get_offset(0, MAX_ROWS as u8 - 1);
    }

    set_cursor_offset(offset);
    offset
}

fn set_cursor_offset(offset: isize) {
    let offset = offset / 2;
    ports::port_byte_out(PORT_SCREEN_CTRL, CTRL_CURSOR_HI);
    ports::port_byte_out(PORT_SCREEN_DATA, (offset >> 8) as u8);
    ports::port_byte_out(PORT_SCREEN_CTRL, CTRL_CURSOR_LO);
    ports::port_byte_out(PORT_SCREEN_DATA, (offset & 0xff) as u8);
}

fn get_cursor_offset() -> isize {
    ports::port_byte_out(PORT_SCREEN_CTRL, CTRL_CURSOR_HI);
    let mut offset = (ports::port_byte_in(PORT_SCREEN_DATA) as isize) << 8;
    ports::port_byte_out(PORT_SCREEN_CTRL, CTRL_CURSOR_LO);
    offset += ports::port_byte_in(PORT_SCREEN_DATA) as isize;
    offset * 2
}

fn get_offset(col: u8, row: u8) -> isize {
    2 * (row as isize * MAX_COLS + col as isize)
}

fn get_offset_row(offset: isize) -> u8 {
    (offset / (2 * MAX_COLS)) as u8
}

fn get_offset_col(offset: isize) -> u8 {
    ((offset - (get_offset_row(offset) as isize * 2 * MAX_COLS)) / 2) as u8
}
