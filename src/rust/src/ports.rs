pub fn port_byte_in(port: u16) -> u8 {
    let b: u8;
    unsafe {
        asm!("in %dx, %al" : "={al}" (b) : "{dx}" (port));
    }
    b
}

pub fn port_word_in(port: u16) -> u16 {
    let w: u16;
    unsafe {
        asm!("in %dx, %ax" : "={ax}" (w) : "{dx}" (port));
    }
    w
}

pub fn port_byte_out(port: u16, b: u8) {
    unsafe {
        asm!("out %al, %dx" : : "{al}" (b) "{dx}" (port));
    }
}

pub fn port_word_out(port: u16, w: u16) {
    unsafe {
        asm!("out %ax, %dx" : : "{ax}" (w) "{dx}" (port));
    }
}