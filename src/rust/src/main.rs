#![no_std]
#![no_main]
#![feature(asm)]
#![feature(const_transmute)]
#![feature(naked_functions)]
#![allow(dead_code)]
use core::panic::PanicInfo;

mod cpu;
mod ports;
mod screen;
mod util;

use screen::{clear_screen, kprint_at, kprint, ScreenLoc};

#[no_mangle]
pub extern "C" fn _start() -> ! {
    clear_screen();
    kprint_at(b"X", ScreenLoc { col: 1, row: 6 });
    kprint_at(b"This text spans multiple lines", ScreenLoc { col: 75, row: 10 });
    kprint_at(b"There is a line\nbreak", ScreenLoc { col: 0, row: 20 });
    kprint(b"There is a line\nbreak");
    kprint_at(b"What happens when we run out of space?", ScreenLoc { col: 45, row: 24 });
    kprint(b"Another line\nwith breaks\n continues");

    cpu::isr_install();
    unsafe {
        asm!(r"
        int $$2
        int $$3
        " : : : : "volatile");
    }

    loop {}
}

#[panic_handler]
fn panic(_info: &PanicInfo) -> ! {
    loop {}
}
