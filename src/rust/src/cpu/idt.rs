use core::mem::{size_of, transmute};

/// Segment selectors
const KERNEL_CS: u16 = 0x0008;

/// Number of interrupt descriptors
const IDT_ENTRIES: usize = 256;

/// How every interrupt gate (handler) is defined
#[repr(C, packed)]
#[derive(Clone, Copy)]
struct IdtGate {
    /// Lower 16 bits of handler function address
    low_offset: u16,
    /// Kernel segment selector
    sel: u16,
    always0: u8,
    /// First byte
    /// Bit 7: "Interrupt is present"
    /// Bits 6-5: Privilege level of caller (0=kernel..3=user)
    /// Bit 4: Set to 0 for interrupt gates
    /// Bits 3-0: bits 1110 = decimal 14 = "32 bit interrupt gate"
    flags: u8,
    /// Higher 16 bits of handler function address
    high_offset: u16, 
}

/// A pointer to the array of interrupt handlers. Assembly instruction 'lidt' will read it
#[repr(C, packed)]
#[derive(Clone, Copy)]
struct IdtRegister {
    limit: u16,
    base: u32,
} 

static mut IDT: [Option<IdtGate>; IDT_ENTRIES] = [None; IDT_ENTRIES];
static mut IDT_REG: Option<IdtRegister> = None;

pub fn set_idt_gate(n: usize, handler: fn() -> ()) {
    let handler = handler as *const fn() as usize as u32;
    let idt = IdtGate {
        low_offset: lo_16!(handler),
        sel: KERNEL_CS,
        always0: 0,
        flags: 0x8e,
        high_offset: hi_16!(handler),
    };
    unsafe {
        IDT[n] = Some(idt);
    }
}

pub fn set_idt() {
    unsafe {
        IDT_REG = Some(IdtRegister {
            limit: ((IDT_ENTRIES as usize) * size_of::<IdtGate>() - 1) as u16,
            base: &(IDT[0]) as *const _ as usize as u32,
        });

        // Don't make the mistake of loading &idt -- always load &idt_reg
        asm!("lidtl $0" : : "m"(&IDT_REG) : : "volatile");
    }
}