use super::Registers;
use super::idt::{set_idt, set_idt_gate};
use crate::screen::kprint;
use crate::util::itoa;

#[no_mangle]
#[naked]
pub extern "C" fn isr_handler(/*r: Registers*/) {
    kprint(b"received interrupt: ");
    //let s = itoa(r.int_no as i32);
    //kprint(&s);
    kprint(b"\n");
    //kprint(exception_messages[r.int_no]);
    kprint(b"\n");
}

// To print the message which defines every exception
/*static exception_messages: = [
    b"Division By Zero",
    b"Debug",
    b"Non Maskable Interrupt",
    b"Breakpoint",
    b"Into Detected Overflow",
    b"Out of Bounds",
    b"Invalid Opcode",
    b"No Coprocessor",

    b"Double Fault",
    b"Coprocessor Segment Overrun",
    b"Bad TSS",
    b"Segment Not Present",
    b"Stack Fault",
    b"General Protection Fault",
    b"Page Fault",
    b"Unknown Interrupt",

    b"Coprocessor Fault",
    b"Alignment Check",
    b"Machine Check",
    b"Reserved",
    b"Reserved",
    b"Reserved",
    b"Reserved",
    b"Reserved",

    b"Reserved",
    b"Reserved",
    b"Reserved",
    b"Reserved",
    b"Reserved",
    b"Reserved",
    b"Reserved",
    b"Reserved"
];*/

pub fn isr_install() {
    set_idt_gate(0, isr0);
    set_idt_gate(1, isr1);
    set_idt_gate(2, isr2);
    set_idt_gate(3, isr3);
    set_idt_gate(4, isr4);
    set_idt_gate(5, isr5);
    set_idt_gate(6, isr6);
    set_idt_gate(7, isr7);
    set_idt_gate(8, isr8);
    set_idt_gate(9, isr9);
    set_idt_gate(10, isr10);
    set_idt_gate(11, isr11);
    set_idt_gate(12, isr12);
    set_idt_gate(13, isr13);
    set_idt_gate(14, isr14);
    set_idt_gate(15, isr15);
    set_idt_gate(16, isr16);
    set_idt_gate(17, isr17);
    set_idt_gate(18, isr18);
    set_idt_gate(19, isr19);
    set_idt_gate(20, isr20);
    set_idt_gate(21, isr21);
    set_idt_gate(22, isr22);
    set_idt_gate(23, isr23);
    set_idt_gate(24, isr24);
    set_idt_gate(25, isr25);
    set_idt_gate(26, isr26);
    set_idt_gate(27, isr27);
    set_idt_gate(28, isr28);
    set_idt_gate(29, isr29);
    set_idt_gate(30, isr30);
    set_idt_gate(31, isr31);
    set_idt();
}

macro_rules! isr_prefix {
    ($isr:expr) => {
        unsafe {
            asm!(r"
            cli
            pushw $0
            " : : "n"($isr) : : "volatile");
        }
    }
}

macro_rules! isr_common_stub {
    () => {
        unsafe {
            // 1. Save CPU state
            asm!(r"
            pusha // Pushes edi,esi,ebp,esp,ebx,edx,ecx,eax
            mov %ds, %ax // Lower 16-bits of eax = ds.
            push %eax // save the data segment descriptor
            mov $$0x10, %ax // kernel data segment descriptor
            mov %ax, %ds
            mov %ax, %es
            mov %ax, %fs
            mov %ax, %gs
            // 2. Call C handler
            call isr_handler
            // 3. Restore state
            pop %eax 
            mov %ax, %ds
            mov %ax, %es
            mov %ax, %fs
            mov %ax, %gs
            popa
            add $$8, %esp // Cleans up the pushed error code and pushed ISR number
            sti
            iret // pops 5 things at once: CS, EIP, EFLAGS, SS, and ESP
            " : : : : "volatile");
        }
    };
}

/// Divide by Zero
fn isr0() {
    isr_prefix!(0);
    isr_common_stub!();
}

/// Debug Exception
fn isr1() {
    isr_prefix!(1);
    isr_common_stub!();

}

/// Non-maskable Interrupt Exception
fn isr2() {
    isr_prefix!(2);
    isr_common_stub!();
}

/// Int 3
fn isr3() {
    isr_prefix!(3);
    isr_common_stub!();
}

fn isr4() {

}

fn isr5() {

}

fn isr6() {

}

fn isr7() {

}

fn isr8() {

}

fn isr9() {

}

fn isr10() {

}

fn isr11() {

}

fn isr12() {

}

fn isr13() {

}

fn isr14() {

}

fn isr15() {

}

fn isr16() {

}

fn isr17() {

}

fn isr18() {

}

fn isr19() {

}

fn isr20() {

}

fn isr21() {

}

fn isr22() {

}

fn isr23() {

}

fn isr24() {

}

fn isr25() {

}

fn isr26() {

}

fn isr27() {

}

fn isr28() {

}

fn isr29() {

}

fn isr30() {

}

fn isr31() {

}