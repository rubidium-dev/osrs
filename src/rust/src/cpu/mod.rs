macro_rules! lo_16 {
    ($address:expr) => {
        ($address & 0xffff) as u16
    };
}

macro_rules! hi_16 {
    ($address:expr) => {
        (($address >> 16) & 0xffff) as u16
    };
}

mod idt;
mod idr;

pub use idr::*;

/// Struct which aggregates many registers
pub struct Registers {
    /// Data segment selector */
    ds: u32,
    /// Registers stored by PUSHA
    edi: u32,
    esi: u32,
    ebp: u32,
    esp: u32,
    ebx: u32,
    edx: u32,
    ecx: u32,
    eax: u32,
    /// Interrupt number (if applicable)
    int_no: u32,
    /// Error code (if applicable)
    err_code: u32,
    /// Registers pushed by the processor automaticallly
    eip: u32,
    cs: u32,
    eflags: u32,
    useresp: u32,
    ss: u32,
}