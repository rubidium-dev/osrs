use crate::screen::OSChar;

pub fn itoa(n: i32) -> [OSChar; 11] {
    let sign = n < 0;
    let mut n = n;
    if sign {
        n *= -1;
    }
    let mut a_str = [0u8; 11];
    let mut digit = 1;
    while n > 0 {
        a_str[digit] = (n % 10) as u8 + b'0';
        digit += 1;
        n /= 10;
    }
    if sign {
        a_str[digit] = b'-';
    }
    a_str
}
