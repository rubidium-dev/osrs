; receiving the data in 'dx'
; for the example we'll assume 'dx'=0x1234
print_hex:
    pusha
    mov cx, 0 ; index variable

; strategy: get the last char of 'dx', convert to ASCII
; numeric ASCII values: '0' (0x30) to '9' (0x39) so just add 0x30 to byte
; for alphabetic characters, A-F: 'A' (0x41) to 'F' (0x46) we'll add 0x40
; then, move the ASCII byte to the correct position on the resulting string
hex_loop:
    cmp cx, 4 ; loop 4 times
    je end

    ; convert last char of 'dx' to ASCII
    mov ax, dx ; use 'ax' as working register
    and ax, 0x000f ; 0x1234 -> 0x0004 by masking first three to zeroes
    add al, 0x30 ; add 0x30 to convert to ASCII
    cmp al, 0x39 ; if > 9, add extra 8 to represent 'A' to 'F'
    jle step2
    add al, 0x07

step2:
    ; 2. get the correct position of the string to place our ASCII char
    ; bx <- base address + string length - index of char
    mov bx, HEX_OUT + 5 ; base + length
    sub bx, cx ; our index var
    mov [bx], al ; copy the ASCII char on 'al' to the position pointed by 'bx'
    ror dx, 4 ; 0x1234 -> 0x4123 -> 0x3412 -> 0x2341 -> 0x1234

    ; increment index and loop
    ;add cx,1
    inc cx
    jmp hex_loop

end:
    ; prepare the parameter and call the function
    ; remember that print receives parameters in 'bx'
    mov bx, HEX_OUT
    call print

    popa
    ret

HEX_OUT:
    db '0x0000', 0 ; reserve memory for our new string