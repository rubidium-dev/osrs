gdt_start:
    ; the GDT starts with NULL 8 bytes
    dd 0x00
    dd 0x00

; GDT for code segment, base = 0x00000000, length = 0x00ffffff
gdt_code:
    dw 0xffff ; segment length, bits 0-15
    dw 0x0 ; segment base, bits 0-15
    db 0x0 ; segment base, bits 16-23
    db 10011010b ; 1st flags, type flags 
                 ; 1010 = 1 for code, 0 for non-conforming, 1 for readable, 0 for accessed
                 ; 1001 = 1 for present, 00 = ring 0, 1 = code/data
    db 11001111b ; 2nd flags (4 bits) + segment length bits 16-19
    db 0x0 ; segment base, bits 24-31

; GDT for data segment, base and length identical to code segment
gdt_data:
    dw 0xffff
    dw 0x0
    db 0x0
    db 10010010b ; 0010 = 0 for data, 0 for non-conforming, 1 for readable, 0 for accessed
    db 11001111b
    db 0x0

gdt_end:


gdt_descriptor:
    dw gdt_end - gdt_start - 1 ; size (16 bit), always one less of its true size
    dd gdt_start ; address (32 bit)

; constants for later use
CODE_SEG equ gdt_code - gdt_start
DATA_SEG equ gdt_data - gdt_start