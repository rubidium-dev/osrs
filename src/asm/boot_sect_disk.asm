; load 'dh' sectors from drive 'dl' into ES:BX
disk_load:
    pusha
    ; reading from disk requires setting specific values in all registers
    ; so we will overwrite our input parameters from 'dx', let's save it
    ; to the stack for later use
    push dx

    mov ah, 0x02 ; ah <- int 0x13 function. 0x02 = read
    mov al, dh ; al <- number of sectors to read (0x01..0x80)
    mov cl, 0x02 ; cl <- sector (0x01..0x11), 0x01 is our boot sector, 0x02 is first 'avail'
    mov ch, 0x00 ; ch < cylinder (0x00..0x3ff), upper 2 bits in cl
    ; dl <- drive number, our call sets it as a parameter and gets it from BIOS
    ; (0 = floppy, 1 = floppy 2, 0x80 = hdd, 0x81 = hdd 2)
    mov dh, 0x00 ; dh <- head number (0x00..0x0f)

    ; [es:bx] <- pointer to buffer where the data will be stored
    ; caller sets it up for us, and it's the standard location for int 0x13
    int 0x13
    jc disk_error ; if error (stored in carry bit)

    pop dx
    cmp al, dh ; BIOS also sets 'al' to the number of sectors read
    jne sectors_error

    popa
    ret

disk_error:
    mov bx, DISK_ERROR
    call print
    call print_nl
    mov dh, ah ; ah = error code, dl = drive that dropped the error
    call print_hex
    jmp disk_loop

sectors_error:
    mov bx, SECTORS_ERROR
    call print

disk_loop:
    jmp $

DISK_ERROR:
    db "Disk read error", 0

SECTORS_ERROR:
    db "Incorrect number of sectors read", 0
