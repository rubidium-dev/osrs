# osrs ("operating system in rust")
A little experiment with booting a microkernel written in Rust. Based mostly on
[Carlos Fenollosa's OS tutorial](https://github.com/cfenollosa/os-tutorial) but also uses some ideas
from [Philipp Opperman's blog OS](https://os.phil-opp.com/).

## Dependencies
To build and run, you will need:
* [Rust](https://www.rust-lang.org/) nightly
    * Use `rustup override set nightly` in the [src/rust](src/rust) folder.
    * Install cargo-xbuild: `cargo install cargo-xbuild`
    * Install the Rust source (we have to rebuild core): `rustup component add rust-src`
* [NASM](https://www.nasm.us/)
* [GNU Make](https://www.gnu.org/software/make/)
    * Optional, but will let you build/run quickly.
* [QEMU](https://www.qemu.org/)
    * Or a different i386 VM host that can boot raw disk images.

## Building & running
A [Makefile](src/Makefile) is included to build dependencies and run the project through QEMU.

To rebuild everything:

```make```

To start QEMU with the currently built `os-image.bin`:

```make run```

🐾